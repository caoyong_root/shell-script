#!/usr/bin/expect

set timeout 5
set host [lindex $argv 0]
set username [lindex $argv 1]
set passwd [lindex $argv 2]
spawn ssh $username@$host
expect {
	"password:"
        {
        send "$passwd\n"
        }
        "(yes/no)?"
        {
        send "yes\n"
        expect "password:"
        send "$passwd\n"
        }
}
expect "#"
send "echo ok"
expect eof
