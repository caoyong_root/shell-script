#!/usr/bin/expect

set timeout 3
set host [lindex $argv 0]
set file [lindex $argv 1]
spawn scp $file $host:/root
expect {
	"password:"
	{
		send "123123\n"
	}
	"(yes/no)?"
	{
		send "yes\n"
		expect "password:"
		send "123123\n"
	}
}
expect eof
spawn ssh $host bash /root/$file
expect "password:"
send "123123\n"
expect eof
spawn scp $host:/root/disk_mem.txt /root/158_disk_mem.txt
expect "password:"
send "123123\n"
expect eof
