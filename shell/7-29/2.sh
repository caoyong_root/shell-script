#!/usr/bin/expect
set timeout 5
set host [lindex $argv 0]
spawn ssh $host
expect {
	       "password:"
        {
        send "123\n"
        }
        "(yes/no)?"
        {
        send "yes\n"
        expect "password:"
        send "123\n"
        }
}
expect eof
