#!/use/sbin/expect

set timeout 1
spawn su - feng
expect "$"
send "ssh-keygen -t rsa"
expect "(/root/.ssh/id_rsa):"
send "\n"
expect "(empty for no passphrase):"
send "\n"
expect "again:"
send "\n"
expect eof
close
spawn su - feng
expect "$"
send "ssh-copy-id -i /home/feng/.ssh/id_rsa.pub feng@192.168.0.158\n"
expect {
"password:"
{
	send "123123\n"
}
"(yes/no)?"
{
	send "yes\n"
	expect "passwoed:"
	send "123123\n"
}
}
expect eof
close
spawn su - feng
expect "$"
send "ssh feng@192.168.0.158 mkdir -p \/lianxi1\/feng\$RANDOM\n"
expect eof
