#!/bin/bash

#!/bin/bash


awk '$0~/Failed/{print   $11}' /var/log/secure|sort|uniq -c|awk '{print $1,$2}' >sshd.txt
ip_addr=($(awk '{print $2}' sshd.txt))
ip_num=($(awk '{print $1}' sshd.txt))

for i in ${!ip_addr[@]}
do
    if egrep "${ip_addr[i]}" /etc/hosts.deny &>/dev/null
    then
        echo "${ip_addr[i]} aready exists"
        echo "${ip_addr[i]}" have access ${ip_num[i]} times 
    else
        echo "################################"
        ((${ip_num[i]} >10 ))&&echo "sshd:${ip_addr[i]}" >>/etc/hosts.deny
        echo "${ip_addr[i]}" have access ${ip_num[i]} times 
    fi  
    
done

##sleep 10min,delete sshd:ip
sleep 600
sed -i '/^ssh/d' /etc/hosts.deny 
