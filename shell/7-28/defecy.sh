#!/bin/bash

awk '$0~/Failed/&&$0~/root/{print $11}' /var/log/secure|sort|uniq -c|awk '{print $1,$2}' >sshd.txt
ip_addr=($(awk '{print $2}' sshd.txt))
ip_num=($(awk '{print $1}' sshd.txt))

for i in ${!ip_addr[@]}
do
	if egrep ${ip_num[i]} /etc/hosts.deny &>/dev/null
	then
		echo "${ip_addr[i]} is already exists"
		echo "${ip_addr[i]} have visit ${ip_num[i]}"
	else
		((${ip_num[i]}>=10))&&echo "sshd:${ip_addr[i]}" >>/etc/hosts.deny
		echo "${ip_addr[i]} have visit ${ip_num[i]}"
	fi
done
(sleep 600;sed -i '/^sshd/d' /etc/hosts.deny)
