#!/bin/bash
#description: config ip

#receive ip infomation
re_info(){
	read -p "please input ip address:" u_ip
	read -p "please input netmask:" u_mask
	read -p "please input ip gateway:" u_gateway
	read -p "please input ip dns:" u_dns
	all_info=(${u_ip} ${u_mask} ${u_gateway} ${u_dns})
}

modify_ip(){
	cp ifcfg-eth0 ifcfg-eth0.bak
	sed -i "/IPADDR/c IPADDR=${u_ip}" ifcfg-eth0
	sed -i "/NETMASK/c NETMASK=${u_mask}" ifcfg-eth0
	sed -i "/GATEWAY/c GATEWAY=${u_gateway}" ifcfg-eth0
	sed -i "/DNS1/c DNS1=${u_dns}" ifcfg-eth0
}

#check ip
check_ip(){
	re_info
	for i in ${!all_info[@]}
	do
	if ((i!=1))
	then
		if echo "${all_info[i]}"|egrep "([1-9]|[1-9][0-9]|1[0-9]{2}|2[0-1][0-9]|22[0-3])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\b){3}" 
		then
			modify_ip
		else
			re_info
		fi
	else
		if echo "${u_mask}"|egrep "(255)(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\b){3}"
		then
			modify_ip
		else
			re_info
		fi
	fi
	done
}
check_ip
#ifdown eth0;ifup eth0
