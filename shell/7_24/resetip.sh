#!/bin/bash

read -p "please input ip:" u_ip
read -p "please input mask:" u_mask
read -p "please input gateway:" u_gw
read -p "please input dns:" u_dns

if echo "${u_ip}"|egrep "([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-1][0-9]|22[0-3])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\b){3}" &>/dev/null
then
	sed -i "/IPADDR/c IPADDR=$u_ip" ifcfg-eth0
	sed -i "/NETMASK/c NETMASK=$u_mask" ifcfg-eth0
	sed -i "/GATEWAY/c GATEWAY=$u_gw" ifcfg-eth0
	sed -i "/DNS1/c DNS1=$u_dns" ifcfg-eth0
else
	echo "ip input is false"
fi
