#!/bin/bash

read -p "please input your grade:" u_grade

if [ -z $u_grade ]
then 
	echo "please input the range of 0-100 number"
	exit 
fi
if echo $u_grade|grep -v "[0-9]" &>/dev/null
then
	echo "please input the range of 0-100 number"
	exit 
fi

if (($u_grade >= 90 && $u_grade <= 100))
then
	echo "your level is A"
elif (($u_grade >= 80 && $u_grade <= 89))
then
	echo "your level is B"
elif (($u_grade >= 70 && $u_grade <= 79))
then
	echo "your lever is C"
elif (($u_grade >= 60 && $u_grade <= 69))
then
	echo "your lever is D"
elif (($u_grade >= 0 && $u_grade <= 59))
then
	echo "your lever is E"
else
	echo "please input the range of 0-100 number"
fi
