#!/bin/bash
now_hour=`date +%H`
now_minute=`date +%M`
if [[ $now_minute>=30 ]]
then
	f_minute=`echo "30+60-$now_minute"|bc`
	f_hour=`echo "16-$now_hour"|bc`
else
	f_minute=`echo "30-$now_minute"|bc`
	f_hour=`echo "17-$now_hour"|bc`
fi
echo "离放学还差$f_hour小时$f_minute分钟"
