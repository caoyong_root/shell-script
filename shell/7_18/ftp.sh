#!/bin//bash

n=0
while :
do
	if nc -z 192.168.0.189 21 &>/dev/null
	then
		echo -e "\e[31mftp server is up!\e[0m"
		sleep 1
	else
		echo "warnning!! 192.168.0.189 ssh server is down"
		echo "`date +%F-%r`,192.168.0.189,down" >>/var/log/monitor_ftp.log
		sleep 1
		((n++))
		if nc -z 192.168.0.189 21 &>/dev/null
		then
			echo -e "\e[32mdown server ftp ${n}s\e[0m"
			echo "`date +%F-%r`,192.168.0.189,up" >>/var/log/monitor_ftp.log
		fi
	fi
done
