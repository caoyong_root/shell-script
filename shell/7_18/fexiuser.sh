#!/bin/bash

>file2
>file3
cat /etc/passwd|cut -d: -f1,3|tr : ' ' >file1
while read user uid
do
	if ((uid==0))
	then 
		echo -e "\e[31m$user是超级用户--->$uid\e[0m"
	elif ((uid>0 && uid <=499 || uid == 65534))
	then
		echo -e "\e[32m$user是系统用户\e[0m--->$uid" >>file2
	else
		echo -e "\e[33m$user是普通用户\e[0m--->$uid"  >>file3
	fi
done<file1
cat -n  file2 file3
