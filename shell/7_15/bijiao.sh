#!/bin/bash

read -p "input two float number:" num1 num2

if (( `echo "$num1" - "$num2"|bc` == 0 )) &>/dev/null
then 
	echo "$num1 = $num2"
else
	if (($(echo "$num1>$num2"|bc)!=0))
	then
		echo "$num1 > $num2"
	else
		echo "$num1 < $num2"
	fi
fi
