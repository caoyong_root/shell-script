#!/bin/bash

used_mem=`free -m|egrep "^M"|awk '{print $3}'`
total_mem=`free -m|egrep "^M"|awk '{print $2}'`

(( used_mem*100/total_mem >80 ))&&echo -e "\e[31mthe memory use more than 80%\e[0m"||echo -e "\e[31mthe memory use ${used_mem}M\e[0m"
