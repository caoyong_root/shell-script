#!/bin/bash

read -p "please input user's name:" u_name
if id ${u_name} &>/dev/null
then
	for i in `seq 3`
	do
	read -p "please input password:" u_pwd
	if (( ${#u_pwd} >= 8 ))
	then
		if echo $u_pwd|egrep [0-9]|egrep [a-Z] &>/dev/null
		then
			echo "$u_pwd"|passwd $u_name --stdin &>/dev/null
			echo -e "\e[31mset password to $u_name ok\e[0m"
			exit
		else
			echo -e "\e[32m$u_pwd is not include character or digit, please retype\e[0m"
		fi
	else 
		echo -e "\e[33mthe length of $u_name password id lesser than 8 characters\e[0m"
	fi
	done
else
	echo -e "\e[34mthe user $u_name is not exists\e[0m"
fi
