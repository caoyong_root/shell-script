#!/bin/bash

menu(){
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	echo -e "\e[31m\tgeshouxinxiguanli\e[0m"
	echo -e "\e[32m1.add singer\e[0m"
	echo -e "\e[33m2.query singer\e[0m"
	echo -e "\e[34m3.exit\e[0m"
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	read -p "please input your choice:" option
}

#1.add singer
add_singer(){
	echo -e "\e[35mplease input a singer's info:\e[0m" 
	read -p "name:" s_name
	singers=(`cat singers.txt|egrep "^${s_name}"`)
	if [[ ${s_name} == ${singers[0]} ]]
	then
		echo -e "\e[31mthe singer is exists\e[0m"
	else
		read -p "sex:" s_sex
		read -p "address:" s_addr
		read -p "type:" s_type
		read -p "year:" s_year
		read -p "product:" s_pro
		declare -A singer
		singer=([name]=${s_name} [sex]=${s_sex} [address]=${s_addr} [sing_type]=${s_type} [year]=${s_year} [product]=${s_pro})
		echo ${singer[@]} >>singers.txt
		echo -e "\e[36madd the singer ${s_name} ok\e[0m"
	fi
}

#2.query singer
query_singer(){
	read -p "please input a singer's name:" s_name1
	singers=(`cat singers.txt|egrep "^${s_name1}"`)
	if [[ ${s_name1} == ${singers[0]} ]]
	then
		echo -e "\e[34m${singers[@]}\e[0m"
	else
		echo -e "\e[31mthe singer is not exists\e[0m"
	fi
}

main(){
	while :
	do
		menu
		case $option in 
		1)
			add_singer
			;;
		2)
			query_singer
			;;
		3)
			exit
			;;
		*)
			exit
			;;
		esac
	done
}
main
