#!/bin/bash

read -p "please input 2 numbers:" a b

eq_result=`echo "$a - $b"|bc`
if [[ $eq_result == 0 ]]
then
	echo "$a=$b"
	exit
fi

result=`echo "$a>$b"|bc` 
if ((result == 0)) 
then
	echo "$a < $b"
else
	echo "$a > $b"
fi
