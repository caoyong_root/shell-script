#!/bin/bash

>/etc/yum.repos.d/yum.repo

[ -d /yum ]||mkdir /yum
mount /dev/cdrom /yum

cd /etc/yum.repos.d

[ -d backup ]||mkdir backup
mv CentOS* backup

cat > /yum.repo <<EOF
[local_yum]
name=local yum
baseurl=file:///yum
enabled=1
gpgcheck=0
EOF

yum clean all
