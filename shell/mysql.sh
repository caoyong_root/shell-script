#!/bin/bash

mysql -uroot -p123123 <<EOF
	create database sanle;
	use sanle;
	create table student(id int(4),name varchar(20),sex varchar(10),address varchar(40),phone varchar(20));
	desc student;
	insert into student(id,name,sex,address,phone) values(1,"Tom","male","nongda","1234");
	insert into student(id,name,sex,address,phone) values(2,"Rose","female","yueyang","33455");
	insert into student(id,name,sex,address,phone) values(3,"Kurry","male","changda","6734");
	select * from student;
	quit
EOF
