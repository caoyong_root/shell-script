#!/bin/bash
#function: create,delete,modify,reset,password,select user's information
#author:caoyong
#company:sanle learning
#date:2017-7-20
#mail:752151766@qq.com
#version:1.0

#menu
menu(){
        clear
        echo -e "\t\e[31m*****the system user's admin menu*****\e[0m"
        echo -e "\t\t\e[32m1.add user\e[0m"
        echo -e "\t\t\e[34m2.del user\e[0m"
        echo -e "\t\t\e[35m3.query user info\e[0m"
        echo -e "\t\t\e[36m4.reset user's passwd\e[0m"
        echo -e "\t\t\e[35m5.modify user's info\e[0m"
        echo -e "\t\t\e[34m6.show all user's info\e[0m"
        echo -e "\t\t\e[33m7.exit\e[0m"
        read -p "please input your chiose:" u_option
}

#1.add user
add_user(){
        read -p "please input user's name:" u_name1
        if id $u_name1 &>/dev/null
        then
                echo -e "\e[31m$u_name1 already exists\e[0m"
        else
		useradd $u_name1
                echo -e "\e[31m@^@ Congratulation! Add user success ! @^@\e[0m"
        fi
}

#2.del_user
del_user(){
        read -p "please input user's name:" u_name2
        if id $u_name2 &>/dev/null
        then
                userdel -r $u_name2
                echo -e "\e[31m@^@ Congratulation! Delete user success ! @^@\e[0m"
        else
                echo
                echo -e "\e[31m$u_name2 not exists\e[0m"
        fi
}

#3.query_user_info
query_user_info(){
        read -p "please input user's name:" u_name3
        if id $u_name3 &>/dev/null
        then
                cat /etc/passwd|egrep "^$u_name3"|awk -F: '{print "name:"$1,"uid:"$3,"gid:"$4,"home directory:"$6}'
		passwdll=`cat /etc/shadow |egrep "^root"|awk -F ":" '{print $2}'`
		if [[ "$passwdll" == '*' || "$passwdll" == '!!' ]]
		then
			echo -e "\033[36m the user is not secure\033[0m"
		else
			echo -e "\033[36m the user is secure\033[0m"
		fi
        else
                echo -e "\e[31m$u_name3 is not exists\e[0m"
        fi
}

#4.reset password
reset_user_pwd(){
	read -p "please input user's name:" u_name4
	if id $u_name4 &>/dev/null
	then
		read -p "please input password:" u_pwd
		echo "$u_pwd"|passwd $u_name4 --stdin
	else
		echo -e "\e[31m$u_name4 is not exists\e[0m"
	fi
}

#5.modify user info
modify_user_info(){
	read -p "please input user's name:" u_name5
	if id $u_name4 &>/dev/null
        then
                echo -e "\e[32m1.modify uid\e[0m"
                echo -e "\e[33m2.modify gid\e[0m"
		echo -e "\e[34m3.modify home directory\e[0m"
		read -p  "please input your choice:" choice2
		case $choice2 in
		1)
			id $u_name5
			read -p "please input a new uid:" u_id
			while :
			do
			if usermod -u $u_id $u_name5 &>/dev/null
			then
				id $u_name5
				echo -e "\e[31mmodify uid ok\e[0m"
				break
			else
				echo -e "\e[31mthe uid is already exists\e[0m"
				read -p "please input a new uid:" u_id
			fi
			done
			;;
		2)
			id $u_name
			read -p "please input a new uid:" g_id
			usermod -g $g_id $u_name5
			id $u_name5
			echo "modify gid ok"
			;;
		3)
			cat /etc/passwd|egrep "^$u_name5"|awk -F: '{print $6}'
			read -p "please input a new home directory:" home_dir
			[ -d $home_dir ]&&echo "the directory is already exists"||mkdir $home_dir
			usermod -d $home_dir $u_name5
			cat /etc/passwd|egrep "^$u_name5"|awk -F: '{print $6}'
			echo "modify home directory ok"
			;;
		*)
			exit
			;;	
		esac
        else
                echo -e "\e[31m$u_name4 is not exists\e[0m"
        fi
}

#6.show all user info
show_user_info(){
	n1=`cat /etc/passwd |wc -l`
	n2=`cat /etc/passwd |awk -F: '$3<500&&$3>0{print $1}'|wc -l`
	n3=` cat /etc/passwd |awk -F: '$3>499{print $1}'|wc -l`
	echo -e "\e[32m所有的用户信息如下 ：\e[0m"
	echo -e "\e[33m用户的总的数量为：$n1\e[0m"
	echo -e "\e[33m超级用户的数量为： 1\e[0m" 
	echo -e "\e[34m系统用户的个数为：$n3\e[0m" 
 	echo -e "\e[35m程序用户的个数为：$n2\e[0m"
}

#main function
main(){
while :
do
        menu
        case $u_option in
        1)
                add_user
                ;;
        2)
                del_user
                ;;
        3)
                query_user_info
                ;;
        4)
                reset_user_pwd
                ;;
        5)
                modify_user_info
                ;;
        6)
                show_user_info
                ;;
        7)
                exit
		;;
	*)
		exit
		;;
        esac
read -p "please press any key to continue"
done
}
main

