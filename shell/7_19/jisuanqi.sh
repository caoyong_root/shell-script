#!/bin/bash

#menu
menu(){
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	echo -e "@\t1.add加法运算"
	echo -e "@\t2.minus减法运算"
	echo -e "@\t3.multiplication乘法运算"
	echo -e "@\t4.division除法运算"
	echo -e "@\t5.compare比较数值大小"
	echo -e "@\t6.exit"
	echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
	read -p "please input your choice:" option
}

#add
add(){
	read -p "请输入两个整数或小数:" a1 b1
	echo "$a1 + $b1 = `echo "$a1 + $b1"|bc`"
}

#minus
minus(){
	read -p "请输入两个整数或小数:" a2 b2
	echo "$a2 - $b2 = `echo "$a2 - $b2"|bc`"
}

#multiplication
mul(){
	read -p "请输入两个整数或小数:" a3 b3
	echo "$a3 * $b3 = `echo "$a3 * $b3"|bc`"
}

#division
div(){
	read -p "请输入两个整数或小数:" a4 b4
	c=$(printf "%.2f" `echo "scale=2;${a4}/${b4}"|bc`)
	echo "$a4 / $b4 = $c"
}

#compare
compare(){
	read -p "please input 2 numbers:" a b

	eq_result=`echo "$a - $b"|bc`
	if [[ $eq_result == 0 ]]
	then
		echo "$a = $b"
		exit
	fi

	result=`echo "$a>$b"|bc`
	if ((result == 0))
	then
		echo "$a < $b"
	else
		echo "$a > $b"
	fi
}

#main
main(){
while :
do
	clear
	menu
	case $option in
	1)
		add
		;;
	2)
		minus
		;;
	3)
		mul
		;;
	4)
		div
		;;
	5)
		compare
		;;
	6)
		exit
		;;
	*)
		exit
		;;
	esac
	read  -n1 -p "please input any key to continue"
done
}

main
