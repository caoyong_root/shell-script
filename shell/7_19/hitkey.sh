#!/bin/bash

read -n1 -p "请输入一个字符,并按Enter键确认:" KEY
case "$KEY" in
	[a-Z])
		echo "您输入的是字母。"
		;;
	[0-9])
		echo "您输入的是数字。"
		;;
	*)
		echo "您输入的是空格、功能键或者其他控制字符。"
		;;
esac
