#!/bin/bash

>/var/log/monitor_ftp.log

server_ftp=192.168.0.192
flag=1

#judge the ftp server is down or up
while :
do
	if nc -z 192.168.0.192 21 &>/dev/null
	then
		echo -e "\e[32m$server_ftp is running\e[0m"
		if ((flag==1))
		then
			echo -e "`date +'%F_%T'`\t$server_ftp server ftp is up" >>/var/log/monitor_ftp.log
			flag=0
		fi
	else
		echo -e "\e[31mwarnning!! $server_ftp ftp server is down\e[0m"
		if ((flag==0))
		then
			echo -e "`date +'%F_%T'`\t$server_ftp is down" >>/var/log/monitor_ftp.log
			flag=1
		fi
	fi
	sleep 1
done
