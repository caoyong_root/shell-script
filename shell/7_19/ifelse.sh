#!/bin/bash

read -n1 -p "请输入一个字符，并按Enter键确认：" KEY

if [[ $KEY == [a-Z] ]]
then
	echo "您输入的是字母。"
elif [[ $KEY == [0-9] ]]
then 
	echo "您输入的是数字。"
else
	echo "您输入的是空格、功能键或者其他控制字符。"
fi
