#!/bin/bash

down_count=`cat /var/log/monitor_ftp.log |tail -n +2|grep "down$"|wc -l`

cat /var/log/monitor_ftp.log |tail -n +2|grep "up$"|awk '{print $1}'|tr "_" " " >up.txt
cat /var/log/monitor_ftp.log |tail -n +2|grep "down$"|awk '{print $1}'|tr "_" " " >down.txt
while read line
do
	up[i++]=$(date +%s -d "$line")
done<up.txt
#echo ${up[@]}
while read line
do
	down[j++]=$(date +%s -d "$line")
done<down.txt
#echo ${down[@]}

n=1
for i in ${!down[@]}
do
	echo -e "$n\t interval time is $((${up[i]} - ${down[i]})) seconds"
	((n++))
done 
echo "#######################"
echo "total down count:$down_count"
echo "#######################"
