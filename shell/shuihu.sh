#!/bin/bash

echo "the first chair: $1"
echo "the second chair: $2"
echo "the third chair: $3"
echo "the fourth chair: $4"
echo "the fifth chair: $5"
echo "the sixth chair: $6"

echo "the name of file is $0"

echo "the number of heroes:$#"
echo "there are heroes name:$@"
echo "#######################"
echo "there are heroes name:$*"

j=1
for i in "$@"
do
	echo "$j is $i"
	((j++))
done
