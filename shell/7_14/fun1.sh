#!/bin/bash

#menu list and receive inputcontent
menu(){
	echo "1.add user"
	echo "2.del user"
	echo "3.exit"
	read -p "please input your choice:" choice
}

#add system user
add_user(){
	read -p "please input user's name:" u_name
	if id ${u_name} &>/dev/null
	then 
		echo "the user is already exists"
	else
		useradd ${u_name}	
		echo "add user $u_name ok"
	fi
}

#delete user
del_user(){
	read -p "please input del user's name:" d_u_name
	if id ${d_u_name} &>/dev/null
	then
		userdel -r ${d_u_name}
		echo "del user ${d_u_name} ok"
	else
		echo "the user is not exists"
	fi
}

main(){
	while :
	do
		clear
		menu
		case $choice in 
		1)
			add_user
			;;
		2)
			del_user
			;;
		3)
			exit
			;;
		*)
			exit
			;;
		esac
		read -p "input any key to continue"
	done
}
main
