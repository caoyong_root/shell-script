#!/bin/bash

#generate singer.txt file
cat >singer.txt  <<EOF
ZhouJieLun
LinGG
TaoGG
QiQin
TFBoys
Beyond
May_Day
EOF

#create an array to store singers and random a singer to sing for us
singer=(`cat singer.txt`)
while :
do
	lucky_num=$((RANDOM%${#singer[@]}))
	echo -e "please \e[31m${singer[lucky_num]}\e[0m sing a song for us"
	unset singer[lucky_num]
	singer=(${singer[@]})
	if (( ${#singer[@]} == 0 ))
	then
		echo "all singers have sing over,please run again"
		exit
	else
		read -p "please welcome next singer"
		echo "there are ${#singer[@]} never sing for us,wait a moment"
		echo "#######################################################"
		echo -e "\e[31m${singer[@]}\e[0m"
		echo "#######################################################"
	fi
done
