#!/bin/bash

echo "eth0 --- `ip add|egrep "eth0$"|awk '{print $2}'`"
echo "eth1 --- `ip add|egrep "eth1$"|awk '{print $2}'`"
echo "gateway --- `route -n|tail -n 1|awk '{print $2}'`"
echo "dns --- `cat /etc/resolv.conf |tail -n1|awk '{print $2}'`"
