#!/bin/bash

read -p "add new user:" u_name

if id $u_name &>/dev/null
then
	echo "$u_name is already exists"
else
	read -s -p "user $u_name password:" u_passwd
	useradd $u_name &>/dev/null
	echo $u_passwd|passwd $u_name --stdin &>/dev/null
	echo
	echo "user $u_name is ok"
fi
