#!/bin/bash

[ -d /yum ]||mkdir /yum
mount /dev/cdrom /yum
cd /etc/yum.repos.d
[ -d backup ]||mkdir backup
mv *.repo backup
cat >local_yum.repo <<EOF
[local_yum]
name=local name
baseurl=file:///yum
enabled=1
gpgcheck=0
EOF
yum clean all
yum install mysqld -y
chkconfig mysqld on
