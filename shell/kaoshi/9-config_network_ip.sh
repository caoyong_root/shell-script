#!/bin/bash

menu(){
	echo "1.config hostname"
	echo "2.config ip"
	echo "3.query hostname and ip"
	echo "4.exit"
	read -p "please input your choice:" option
}

#1.config hoatname
cfg_hname(){
	read -p "please input a nwe hostname:" h_name
	sed -i "/HOSTNAME/c HOSTNAME=$h_name" /etc/sysconfig/network
	hostname $h_name
}

#2.config ip
cfg_ip(){
	read -p "please input ip:" ip_addr
	echo $ip_addr|egrep "([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-1][0-9]|22[0-3])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\b){3}"  &>/dev/null
	if (($?==0))
	then
		echo "12312"
	else
		echo "the ip is false"
	fi 
}

#3.query hostname and ip
query_ip_hostname(){
	echo "hostname: `cat /etc/sysconfig/network|awk -F= '/^HOST/ {print $2}'`"
	echo "ip: `cat /etc/sysconfig/network-scripts/ifcfg-eth0|awk -F= '/^IPADDR/ {print $2}'`"
}

main(){
	while :
	do
		menu
		case $option in
		1)
			cfg_hname
			;;
		2)
			cfg_ip
			;;
		3)
			query_ip_hostname
			;;
		4)
			exit
			;;
		*)
			exit
			;;
		esac
	done
}

main
