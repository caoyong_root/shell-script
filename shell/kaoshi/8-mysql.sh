#!/bin/bash

mysql -uroot -p123123 <<EOF
	create database sanle;
	use sanle;
	create table student(id int(5) primary key,name varchar(20),sex varchar(10),address varchar(20),phone varchar(10));
	insert into student(id,name,sex,address,phone) values(1,"Tom","male","nongda","1234");
	insert into student(id,name,sex,address,phone) values(2,"Mary","female","changda","43534");
	insert into student(id,name,sex,address,phone) values(3,"Cary","male","yueyang","34523");
	select * from student;
	quit
EOF
