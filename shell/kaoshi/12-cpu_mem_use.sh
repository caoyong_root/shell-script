#!/bin/bash

echo -e  "\t\e[31mCPU use\e[0m"
ps aux|sort -r -k3|head -11|tail -10|awk '{print "pid:"$2,"name:"$11,"use:"$3}'
echo "###############################"
echo -e "\t\e[32mMEMORY use\e[0m"
ps aux|sort -r -k4|head -11|tail -10|awk '{print "pid:"$2,"name:"$11,"use:"$3}'
