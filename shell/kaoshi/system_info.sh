#!/bin/bash

df -Th|tr -s " " ","
free -m|tr -s " " ","
top -bcn 1|egrep "^Cpu"|tr -d " "
