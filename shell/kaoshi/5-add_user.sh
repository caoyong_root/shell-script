#!/bin/bash

read -p "please input name:" u_name
read -p "please input a number:" u_num
read -p "please input the password:" u_pwd

for i in `seq $u_num`
do
	if id ${u_name}${u_num} &>/dev/null
	then
		echo "${u_name}${i} is already exists!"
		continue
	else
		useradd ${u_name}${i}
		echo "$u_pwd"|passwd ${u_name}${i} --stdin
		cat /etc/passwd|egrep "^${u_name}${i}\b"|awk -F: '{print "name:"$1,"uid:"$3,"gid:"$4,"home directory:"$6,"shell:"$7}'
	fi
done
