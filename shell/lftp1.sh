#!/bin/bash
aa=`cat xferlog |awk '{print $7}'|sort|uniq`
for i in `echo $aa`
do 
	a=`cat xferlog |egrep "\b$i\b"|awk '{print $8}'|paste -s -d"+"|bc`
	f_size=`echo "scale=2;$a/1000/1000"|bc`
	if echo ${f_size}|egrep "^[0-9]" &>/dev/null
	then
		f_size=`echo "${f_size}"`
	else
		f_size=`echo "0${f_size}"`
	fi
	echo "$i download files have ${f_size}M"
done

