#!/bin/bash

size=(`ls -l $1|egrep "^-"|tr -s " "|cut -d " " -f5`)
num=0
total=0
for i in ${!size[@]}
do
	c=${size[$i]}
	d=$((c/1000/1000))
	if (($d>=10))
	then
		((num++))
		total=$((total+d))
	fi
done
echo "more than 10M file's num are:" $num
echo "those file's total size are:${total}M"
